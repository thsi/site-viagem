<!DOCTYPE html>
<html lang="en">
<head>
  <title>.:: Vem Viajar ::.</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="public/css/bootstrap.min.css">
  <script src="public/js/jquery.min.js"></script>
  <script src="public/js/popper.min.js"></script>
  <script src="public/js/bootstrap.min.js"></script>
  <script defer src="public/fontawesome/svg-with-js/js/fa-solid.js"></script>
<script defer src="public/fontawesome/svg-with-js/js/fontawesome-all.js" ></script>
  <style>
    /* remove a margem do navbar */ 
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }
    /* adiciona a cor no botao do rodape */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
	.direita{
    /*cria alinhamento a direita */ 
		float:  right;
	}
	.esquerda{
     /*cria alinhamento a esquerda */
		float:  left;
	}
  .parallax { 
    /* caminho da imagem*/
    background-image: url("public/img/fundo.jpg");

    /* especifica a largura */
    height: 500px; 

    /* cria o efeito parallax */
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.voltar-ao-topo {
            position: fixed;
            bottom: 8em;
            right: 2em;
            text-decoration: none;
            color: white;
            background-color: rgba(0, 0, 0, 0.3);
            font-size: 12px;
            padding: 1em;
            display: none;
        }
        .voltar-ao-topo:hover {
            background-color: rgba(0, 0, 0, 0.6);
        }
  </style>
</head>
<body>

<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});
</script>
<?php 
//faz a inclusao das páginas no diretorio VIEWS
include("views/topo.php");
echo "<b>";include("views/nav.php");echo "</b>";
include("views/anuncios.php");
include("views/modal.php");
?>
<br><br>

<script>
//script para enviar o ID dos serviços para o MODAL
function setaDadosModal(valor) {
    document.getElementById('campo').value = valor;
}
</script>
<script>
//script para suavizar a ancora html
var $doc = $('html, body');
$('a').click(function() {
    $doc.animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
});
</script>
<a href="#" class="voltar-ao-topo">Voltar ao topo</a>
<?php 
//inclui a pagina footer (rodape)
include "views/footer.php";?>

</body>
</html>
