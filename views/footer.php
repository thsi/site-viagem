<footer  id="contato">
<h4 align="center">Contato</h4>   
        <form  action="model/SalvarDados.php" method="post">
        <div class="row">
            <div class="col-sm-4">
                <label for="nome">Nome:</label>            
                <input type="text" class="form-control" name="nome" required>
            </div>
            <div class="col-sm-4">
                <label for="nome">E-mail:</label>            
                <input type="email" class="form-control" name="email" required>
            </div>
            <div class="col-sm-3">
                <label for="telefone">Telefone:</label>
                <input type="text" class="form-control" name="telefone" required>
            </div>           
                <button type="submit" class="btn btn-danger">Enviar</button>
        </div>      
        </form> 
        <script>
        jQuery(document).ready(function() {
            // Exibe ou oculta o botão
            jQuery(window).scroll(function() {
                if (jQuery(this).scrollTop() > 200) {
                    jQuery('.voltar-ao-topo').fadeIn(200);
                } else {
                    jQuery('.voltar-ao-topo').fadeOut(200);
                }
            });
            
            // Faz animação para subir
            jQuery('.voltar-ao-topo').click(function(event) {
                event.preventDefault();
                jQuery('html, body').animate({scrollTop: 0}, 300);
            })
        });
        </script>
</footer>
