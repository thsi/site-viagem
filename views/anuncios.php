<div style="background-color:#B0C4DE">
<br /><h3 style="text-align:center">Pacotes Individuais</h3>
<div class="container" id="pacoteindividual">    
  <div class="row">
    <div class="col-sm-4">
		<div class="card" style="width:300px">
			<img class="card-img-top" src="public/img/ilheus.jpg" alt="Ilheus" style="width:100%">
			<div class="card-body">
			  <h4 class="card-title">Ilhéus - BA</h4>
			  <p class="card-text">Veja o que Ilhéus tem de melhor!</p>
			  <div class="direita">
				<a href="#" onclick="setaDadosModal('1IL020')" data-toggle="modal" data-target="#reserva">
					<i class="fas fa-cart-plus"></i>
				</a>
				</div>
			</div>
		</div>
	</div> 
	<div class="col-sm-4"> 
		<div class="card" style="width:300px">
			<img class="card-img-top" src="public/img/bonde.jpg" alt="Bondinho" style="width:100%">
			<div class="card-body">
			  <h4 class="card-title">Bondinho - RJ</h4>
			  <p class="card-text">Veja o que RJ tem de melhor!</p>
			  <div class="direita">
				<a href="#" onclick="setaDadosModal('1CRJ150')" data-toggle="modal" data-target="#reserva">
					<i class="fas fa-cart-plus"></i>
				</a>
				</div>
			</div>
		</div>
    </div>
	 <div class="col-sm-4"> 
		<div class="card" style="width:300px">
			<img class="card-img-top" src="public/img/bonito_ind_ms.jpg" alt="Bonito" style="width:100%">
			<div class="card-body">
			  <h4 class="card-title">Bonito - MS</h4> 
			  <p class="card-text">Veja o que Bonito tem de melhor!</p>
			  <span class="badge badge-danger"><i class="fas fa-star"></i> Promoção</span>
			  <div class="direita">
				<a href="#" onclick="setaDadosModal('1BN65')" data-toggle="modal" data-target="#reserva">
					<i class="fas fa-cart-plus"></i>
				</a>
				</div>
			</div>
		</div>
    </div>
  </div>
</div><br>
	<h3 style="text-align:center">Pacotes em Grupo</h3>
<div class="container" id="pacotegrupo">    
  <div class="row">
        <div class="col-sm-4">
		<div class="card" style="width:300px">
			<img class="card-img-top" src="public/img/salvador.jpg" alt="Salvador" style="width:100%">
			<div class="card-body">
			  <h4 class="card-title">Salvador - BA</h4>
			  <p class="card-text">Veja o que Salvador tem de melhor!</p>
			  <div class="direita">
				<a href="#" onclick="setaDadosModal('6SA020')" data-toggle="modal" data-target="#reserva">
					<i class="fas fa-cart-plus"></i>
				</a>
				</div>
			</div>
		</div>
	</div> 
	<div class="col-sm-4"> 
		<div class="card" style="width:300px">
			<img class="card-img-top" src="public/img/rj.jpg" alt="Cristo Redentor" style="width:100%">
			<div class="card-body">
			  <h4 class="card-title">Cristo Redentor - RJ</h4>
			  <p class="card-text">Veja o que RJ tem de melhor!</p>
				<span class="badge badge-danger"><i class="fas fa-star"></i> Promoção</span>
			  <div class="direita">
				<a href="#" onclick="setaDadosModal('6RJ150')" data-toggle="modal" data-target="#reserva">
					<i class="fas fa-cart-plus"></i>
				</a>
				</div>
			</div>
		</div>
    </div>
	<div class="col-sm-4"> 
		<div class="card" style="width:300px">
			<img class="card-img-top" src="public/img/bonito_ms.jpg" alt="Bonito" style="width:100%">
			<div class="card-body">
			  <h4 class="card-title">Bonito - MS</h4> 
			  <p class="card-text">Veja o que Bonito tem de melhor!</p>			  
			  <div class="direita">
				<a href="#" onclick="setaDadosModal('6BNG165')" data-toggle="modal" data-target="#reserva">
					<i class="fas fa-cart-plus"></i>
				</a>
				</div>
			</div>
		</div>
    </div>
  </div>
</div>
<br><div class="parallax"></div>
	<div class="container"><br />
        <h4 style="text-align:center" id="servicos"> ALUGUEL DE VEÍCULOS, ESCUNAS E LANCHAS </h4><br>
        <div class="row">
            <div class="col s4 m2" >
              <div class="col-sm-3"> 
								<div class="card" style="width:180px">
									<img class="rounded" src="public/img/onibus.jpg" alt="Salvador" style="width:100%">
									<div class="card-body">
										<h4 class="card-title">Ônibus</h4><small><em>Saiba mais</em></small>
											<div class="class direita">
												<a href="#" class="direita" title="Passeio" data-toggle="popover" data-trigger="hover" 
												data-content="O ônibus possui lugares para até 48 pessoas. Possui seguro viagem, frigobar,
												som ambiente, ar condicionado."> <i class="fas fa-plus-square"></i></a> 
											</div>
									</div>
								</div>
							</div>
            </div>
            <div class="col s4 m2">
              <div class="col-sm-3"> 
								<div class="card" style="width:200px">
									<img class="rounded" src="public/img/microonibus.jpg" alt="Salvador" style="width:100%">
									<div class="card-body">
										<h4 class="card-title">Micro-ônibus</h4><small><em>Saiba mais</em></small>
											<div class="class direita">
												<a href="#" class="direita" title="Passeio" data-toggle="popover" data-trigger="hover" 
												data-content="O micro ônibus possui lugares para até 30 pessoas. Possui seguro viagem,
												som ambiente, ar condicionado."> <i class="fas fa-plus-square"></i></a> 
											</div> 
									</div>
								</div>
							</div>
            </div>
            <div class="col s4 m2">
                <div class="col-sm-3"> 
									<div class="card" style="width:210px">
										<img class="rounded" src="public/img/van.jpg" alt="Salvador" style="width:100%">
										<div class="card-body">
											<h4 class="card-title">Van</h4> <small><em>Saiba mais</em></small>
											<div class="class direita">
												<a href="#" class="direita" title="Passeio" data-toggle="popover" data-trigger="hover" 
												data-content="A Van possui lugares para até 18 pessoas. Possui seguro viagem,
												som ambiente, ar condicionado."> <i class="fas fa-plus-square"></i></a> 
											</div>
										</div>
									</div>
								</div>
            </div>
            <div class="col s4 m2">
                <div class="col-sm-3"> 
									<div class="card" style="width:160px">
										<img class="rounded" src="public/img/barco.jpg" alt="Salvador" style="width:100%">
										<div class="card-body">
											<h4 class="card-title" >Barco</h4><small><em>Saiba mais</em></small>
											<div class="class direita">
												<a href="#" class="direita" title="Passeio" data-toggle="popover" data-trigger="hover" 
												data-content="A escuna possui lugares para até 30 pessoas. Possui coletes salva vidas, 
												cozinha, bar, música ao vivo."> <i class="fas fa-plus-square"></i></a>
											</div>
										</div>
									</div>
								</div>
            </div>
						
        </div>
  </div>
		