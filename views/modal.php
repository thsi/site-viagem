<!-- Modal -->
<div id="reserva" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
			<h4 class="modal-title">Reserve sua viagem</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>        
      </div>
      <div class="modal-body">
        <form class="form-horizontal" action="model/reservar_viagem.php" method="post">
		   <div class="form-group">		      
			<label class="control-label col-sm-2" for="nome">Código:</label>
			<div class="col-sm-10">
			  <input type="text" class="form-control" id="campo" name="codigo"  readonly>
			</div>
		  </div>
		  <div class="form-group">		      
			<label class="control-label col-sm-2" for="nome">Nome:</label>
			<div class="col-sm-10">
			  <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite nome completo" required>
			</div>
		  </div>
		 <div class="form-group">
			<label class="control-label col-sm-2" for="email">Email:</label>
			<div class="col-sm-10">
			  <input type="email" class="form-control" id="email" name="email" placeholder="Digite um email" required>
			</div>
		  </div>
		   <div class="form-group">
			<label class="control-label col-sm-2" for="nome">Telefone:</label>
			<div class="col-sm-10">
			  <input type="text" class="form-control" id="telefone" name="telefone" placeholder="DDD + Número" required>
			</div>
		  </div>
		    <div class="form-group">
			<label class="control-label col-sm-2" for="nome">Período</label>
			<div class="col-sm-4">Início:
			  <input type="date" class="form-control" id="data_ini" name="data_inicio" placeholder="Data" required>
			</div>
			<div class="col-sm-4">Fim:
			  <input type="date" class="form-control" id="data_fim" name="data_fim" placeholder="Data" required>
			</div>
		  </div>
		  
	
      </div>
      <div class="modal-footer">
	  <div class="esquerda">
	   <button type="submit" class="btn btn-success">Solicitar</button></div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
	</form> 
  </div>
</div>